package jogo4emlinha;

import java.util.Scanner;
import java.io.*;

public class Jogador {

    //variavel que vai conter o numero de jogadores a introduzir.
    private int nJIntroduzir = 0;
    //variavel usada para atribuir o id ao jogador.
    private int idJogador = 0;
    //variavel para atribuir o status de Ativo quando se cria um jogador.
    private String statusJogador = "A";
    //variavel que vai conter o nome.
    private String nome;
    //variavel que vai conter a idade.
    private int idade = 0;
    //variavel que vai atribuir por pre defenição o numero de vitorias em jogo 1vs1
    private int jGanhos = 0;
    //variavel que vai atribuir por pre defenição o numero de vitorias em torneios
    private int tGanhos = 0;

    //Criação do scanner para ler o input do utilizador
    Scanner scanner = new Scanner(System.in);
    
    //Construtor da classe.
    public Jogador() {
    }

    /**
     * Metodo para criar jogador 
     * @param g 
     */
    public void criarJogador(guardarJogador g) { 
        //Pergunta quantos jogadores vai inserir.
        System.out.print("Quantos jogadores pretende introduzir ?: ");
        this.nJIntroduzir = scanner.nextInt();

        //Ciclo para verificar se o utilizador insere um valor valido.
        while (nJIntroduzir < 0) {
            System.out.println("Introduza um numero válido: ");
            this.nJIntroduzir = scanner.nextInt();
        }
        
        //Ciclo para introduzir os jogadores
        for (int i = 0; i < nJIntroduzir; i++) {

            // Atribui ao objeto o nome
            System.out.print("Introduza o nome do jogador: ");
            this.nome = scanner.next();

            // Atribui ao objeto a idade
            System.out.print("Introduza o idade do jogador: ");
            this.idade = scanner.nextInt();
            
            //Coloca o idJogador a 0 
            this.idJogador = 0;
            
            //try catch para atribuir id ao jogador
            try {
                //Inicializa um BufferedReader no que está no caminho ficheiroJ
                BufferedReader br = new BufferedReader(new FileReader(g.ficheiroJ()));
                //Enquanto houver linhas no ficehiro 
                while (br.readLine() != null) {
                    //incrementa o a variavel idJogador que vai ser o id atribuido
                    this.idJogador++;
                }
                //Passa como parametro todos os valores the this (status,id,nome,idade,jganhos,tganhos).
                g.guardarJ(this);
                //Termina o BufferedReader.
                br.close();
            } catch (IOException e) {
            }
        }
    }

    
    public int getnJIntroduzir(){
        return this.nJIntroduzir;
    }
    
    public String getstatusJogador(){
        return this.statusJogador;
    }
    
    public int getID() {
        return this.idJogador;
    }

    public String getNome() {
        return this.nome;
    }

    public int getIdade() {
        return this.idade;
    }

    public int getjGanhos() {
        return this.jGanhos;
    }

    public int gettGanhos() {
        return this.tGanhos;
    }
}
