package jogo4emlinha;

import java.util.Scanner;

public class Jogo4EmLinha {

    static Scanner scanner = new Scanner(System.in);


    public static void main(String[] args) {

        //verificadores de input do utilizador
        char opcao;
        boolean verificar = false;

        //Menu do jogo
        System.out.println("Bem-vindo ao jogo 4 em linha");
        System.out.println("As opções assinaladas com // nao estao funcionais");
        System.out.println("Menu");
        System.out.println("1) Jogar 1 vs 1");
        System.out.println("2) Carregar Jogo // ");
        System.out.println("3) Jogar Campeonato //");
        System.out.println("4) Classificações //");
        System.out.println("5) Criar Novo Jogador");
        System.out.println("6) Sair");
        System.out.println("");
        
        //Le qual das opções o utilizador pretende usar 
        System.out.print("Introduza a opção: ");
        opcao = scanner.next().charAt(0);
        do {
            verificar = false;
            //Cria um objeto da classe guardarJogador
            guardarJogador g = new guardarJogador();
            //Invoca o metodo para criar o diretorio
            g.criardirJ();
            //Invoca o metodo para criar o ficheiro txt
            g.criarficheiroJ();
            //Cria um objeto da classe Tabuleiro
            Tabuleiro t = new Tabuleiro();
            
            switch (opcao) {
                case '1':
                    //Invoca o metodo para listar os jogadores ativos e seleciona-los
                    t.escolhadeJogadores();
                    //Invoca o metodo tab
                    t.tab(t);
                    break;
                case '2':
                    
                    //Cria um objeto da classe CarregarJogo
                    CarregarJogo c = new CarregarJogo();
                    //Invoca o metodo CarregarFicheiro para carregar um jogo a meio
                    c.CarregarFicheiro(t);
                    break;
                case '3':
                    break;
                case '4':
                    //Cria um objeto da classe Classificacao
                    Classificacao cl = new Classificacao();
                    //Invoca o metodo orgClassificacao
                    cl.orgClassificacao();
                    break;
                case '5':
                    //Cria um objeto da classe Jogador
                    Jogador j = new Jogador();
                    //Invoca o metodo criarJogador para criar o jogador
                    j.criarJogador(g);
                    break;
                case '6':
                    //Sai do programa
                    System.out.println("Saiu com sucesso");
                    break;
                default:
                    //Caso a opção seja invalida vai pedir uma nova opção
                    System.out.println("Opção invalida");
                    System.out.println("Introduza a opção: ");
                    opcao = scanner.next().charAt(0);
                    verificar = true;
                    break;
            }
        } while (verificar == true);  
    }
}