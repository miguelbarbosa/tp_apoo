package jogo4emlinha;

import java.io.*;

/**
 *
 * @author Samsung
 */
public class guardarJogador {

    /*Variavel que vai conter o caminho do ficheiro onde se encontram guardados
    os jogadores*/
    private File ficheiroJ = null;

    //Construtor da classe
    public guardarJogador() {
   
    }
    /**
     * Metodo para criar um diretorio Jogadores
     */
    public void criardirJ() {
        File dirJ = new File("Jogadores");
        //verifica se o diretorio já existe
        if (!dirJ.exists()) {
            //cria o diretorio
            if (dirJ.mkdir()) {
                System.out.println("Diretorio foi criado!");
                // se ocorrer algum erro avisa
            } else {
                System.out.println("Problema na criação do ficheiro");
            }  
        }
    }

    /**
     * Cria o ficheiro de texto onde vao ser guardados os jogadores
     */
    public void criarficheiroJ() {

        try {
            //Cria o ficheiro dentro do diretorio anteriormente criado
            this.ficheiroJ = new File("Jogadores\\Info_Jogadores.txt");
            //Tenta criar o ficheiro se nao existir
            if (!this.ficheiroJ.exists()){
            ficheiroJ.createNewFile();
            }
        } catch (IOException e) {

        }
    }
    
    /**
     * Metodo para guardar os jogadores no ficheiro de texto
     * @param j 
     */
    public void guardarJ(Jogador j) {
        //Inicia um BufferedWriter no ficheiroJ que vai escrever no ficheiro sem apagar o conteudo que ja la estava
        try (BufferedWriter bw = new BufferedWriter(new FileWriter(ficheiroJ, true))) {
            // Escreve em linha as "caracteristicas" do objeto j recebidos
            bw.write(j.getID() + ";" + j.getstatusJogador() + ";" + j.getNome() + ";"
                    + j.getIdade() + ";" + j.getjGanhos() + ";"
                    + j.gettGanhos());
            //Da um "Enter"
            bw.newLine();
            //Termina o BufferedWriter
            bw.close();
        } catch (IOException e) {
        }
    }

    public File ficheiroJ() {
        return this.ficheiroJ;
    }
}
