/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package jogo4emlinha;

import java.util.Scanner;
import java.io.*;

/**
 *
 * @author Bruno
 */
public class Tabuleiro {
    
    //Vai receber o ID do jogador 1
    private int j1ID = 0;
    //Vai receber o ID do jogador 2
    private int j2ID = 0;
    //Vai receber o Nome do jogador 1
    private String j1Nome;
    //Vai receber o Nome do jogador 2
    private String j2Nome;
    //Apenas um contador
    private int i = 0;
    //Vetor que vai conter as informações do jogador divididas
    private String[] split;
    //Criação do scanner para ler o input do utilizador
    Scanner scanner = new Scanner(System.in);
    
    //Construtor da classe
    public Tabuleiro() {

    }

    /**
     * Methodo para listar jogadores ativos e selecionar quais vão jogar.
     *
     * @param g
     */
    public void escolhadeJogadores() {
        try {
            // variavel i defenida para 0 caso seja necessario voltar a este metodo.
            this.i = 0;

            // ARANJAR O DIRETORIO!
            //criar um scanner linhas.
            Scanner linhas = new Scanner(new File("Jogadores\\Info_Jogadores.txt"));

            /*
              if (!linhas.hasNextLine()){ 
                  System.out.println("Por favor crie jogadores primeiro. "); 
                  break; 
              }
             */
            //Ciclo while para contar as linhas do ficheiro dos Info_Jogadores.
            while (linhas.hasNextLine()) {
                //Divide a linha nos ";"
                this.split = linhas.nextLine().split(";");
                //Conta apenas os jogadores marcados como "A".
                if (this.split[1].equals("A")) {
                    this.i++;
                }
            }
            //Cria um vetor com this.i "espaços".
            String[] vetJ = new String[this.i];
            int[] vetID = new int[this.i];
            String[] vetNome = new String[this.i];

            //Volta a criar um scanner linhas2 por nao podermos usar novamente o anterior ( sem flush).
            Scanner linhas2 = new Scanner(new File("Jogadores\\Info_Jogadores.txt"));

            this.i = 0;

            //Ciclo while para guardar os jogadores "A".
            while (linhas2.hasNextLine()) {
                //Divide novamente cada linha nos ";".
                this.split = linhas2.nextLine().split(";");
                //Se jogadore for marcado como "A" adiciona-o ao vetor.
                if (this.split[1].equals("A")) {
                    //Adiciona no vetJ o ID e o Nome do jogador.
                    vetJ[this.i] = "ID: " + this.split[0] + " >> Nome: " + this.split[2];
                    //Adiciona no vetID o id dos jogadores em formato int.
                    vetID[this.i] = Integer.parseInt(this.split[0]);
                    //
                    vetNome[this.i] = this.split[2];
                    this.i++;
                }
            }
            //Imprime no ecra todos os jogadores anteriormente selecionados no vetJ.
            for (int j = 0; j < vetJ.length; j++) {
                System.out.println(vetJ[j]);
            }

            //Introdução e verificação do 1º jogador.
            System.out.print("Introduza o id do 1º jogador: ");
            this.j1ID = scanner.nextInt();
            this.i = 0;
            //Ciclo while para verificar se o jogador existe.
            while (this.j1ID != vetID[this.i]) {
                //verifica se já percorreu o vetID até ao fim. 
                if (this.i == vetID.length - 1) {
                    System.out.println("ID invalido!");
                    System.out.print("Introduza o id do 1º jogador: ");
                    this.j1ID = scanner.nextInt();
                    this.i = -1;
                }
                this.i++;
            }
            this.i = 0;
            //Ciclo while para atribuir o nome ao jogador.
            while (this.j1ID != vetID[this.i]) {
                //Guarda o nome do jogador.
                vetNome[this.i] = this.split[2];
                this.i++;
            }
            //Define o j1Nome.
            this.j1Nome = vetNome[this.i];
            System.out.println(this.j1Nome);

            //Introdução e verificação do 2º jogador.
            System.out.print("Introduza o id do 2º jogador: ");
            this.j2ID = scanner.nextInt();
            this.i = 0;
            while (this.j2ID != vetID[this.i]) {
                if (this.i == vetID.length - 1) {
                    System.out.println("ID invalido!");
                    System.out.print("Introduza o id do 2º jogador: ");
                    this.j2ID = scanner.nextInt();
                    this.i = -1;
                }
                this.i++;
            }
            this.i = 0;
            while (this.j2ID != vetID[this.i]) {
                //Guarda o nome do jogador.
                vetNome[this.i] = this.split[2];
                this.i++;
            }
            this.j2Nome = vetNome[this.i];
            System.out.println(this.j2Nome);

            //Verifica se introduziu 2x o mesmo jogador.
            while (this.j1ID == this.j2ID) {
                System.out.println("");
                System.out.println("Introduza 2 jogadores diferentes: ");
                this.i = 0;
                /* Define ambos os jogadores a -1 para dar oportunidade de
                introduzir novamente ambos os jogadores.*/
                this.j1ID = -1;
                this.j2ID = -1;
                while (this.j1ID != vetID[this.i]) {
                    if (this.i == vetID.length - 1) {
                        System.out.println("ID invalido!");
                        System.out.print("Introduza o id do 1º jogador: ");
                        this.j1ID = scanner.nextInt();
                        this.i = -1;
                    }
                    this.i++;
                }
                this.i = 0;
                while (this.j1ID != vetID[this.i]) {
                    vetNome[this.i] = this.split[2];
                    this.i++;
                }
                this.j1Nome = vetNome[this.i];
                System.out.println(this.j1Nome);

                System.out.print("Introduza o id do 2º jogador: ");
                this.j2ID = scanner.nextInt();
                this.i = 0;
                while (this.j2ID != vetID[this.i]) {
                    if (this.i == vetID.length - 1) {
                        System.out.println("ID invalido!");
                        System.out.print("Introduza o id do 2º jogador: ");
                        this.j2ID = scanner.nextInt();
                        this.i = -1;
                    }
                    this.i++;
                }
                this.i = 0;
                while (this.j2ID != vetID[this.i]) {
                    vetNome[this.i] = this.split[2];
                    this.i++;
                }
                this.j2Nome = vetNome[this.i];
                System.out.println(this.j2Nome);
            }
            //Fecha os scanners anteriormente abertos.
            linhas.close();
            linhas2.close();
        } catch (IOException e) {
        }

    }

    /*--
        Vai criar o padrão da matriz
    */
    public static String[][] criarPadrao() {
        //Criar a Matriz 
        String[][] Mat = new String[7][15];

        //Percorre as linhas de cima para baixo
        for (int i = 0; i < Mat.length; i++) {

            //Percorre cada coluna da esquerda para a direita
            for (int j = 0; j < Mat[i].length; j++) {
                //Percorre cada coluna da esquerda para a direita, com a borda e a 
                //coluna impar entre elas, que estarão vazias ou terão um numero 
                if (j % 2 == 0) {
                    Mat[i][j] = "|";
                } else {
                    Mat[i][j] = " ";
                }

                //Criar os ----- no fim
                if (i == 6) {
                    Mat[i][j] = "-";
                }
            }

        }
        return Mat;
    }

    /*--
        Digita no ecra a matriz do jogo
    */
    public static void imprimirPadrao(String[][] Mat) {
        for (int i = 0; i < Mat.length; i++) {
            for (int j = 0; j < Mat[i].length; j++) {
                System.out.print(Mat[i][j]);
            }
            System.out.println();
        }
    }


    //Insere na linha vazia mais baixa da coluna selecionada para o primeiro jogador
    public static void inserirLinhaPrimeiroJogador(String[][] Mat, Tabuleiro t) {

        int scan = 0;
        Scanner scanner = new Scanner(System.in);
        //Le o valor introduzido pelo utilizador
        System.out.println(t.getj1Nome() + " insira uma coluna: ");
        scan = scanner.nextInt();

        // Se o utilizador digitar 0, então o vai ser guardado o jogo e a aplicação será fechada
        if (scan == 0){

        if (scan == 0) {

            System.out.println("Fechar");
            String eol = System.getProperty("line.separator");

            BufferedWriter bw = null;
            try {

                String conteudo = Mat[0][0] + Mat[0][1] + Mat[0][2] + Mat[0][3] + Mat[0][4] + Mat[0][5] + Mat[0][6] + Mat[0][7] + Mat[0][8] + Mat[0][9] + Mat[0][10] + Mat[0][11] + Mat[0][12] + Mat[0][13] + Mat[0][14] + eol
                                 + Mat[1][0] + Mat[1][1] + Mat[1][2] + Mat[1][3] + Mat[1][4] + Mat[1][5] + Mat[1][6] + Mat[1][7] + Mat[1][8] + Mat[1][9] + Mat[1][10] + Mat[1][11] + Mat[1][12] + Mat[1][13] + Mat[1][14] + eol
                                 + Mat[2][0] + Mat[2][1] + Mat[2][2] + Mat[2][3] + Mat[2][4] + Mat[2][5] + Mat[2][6] + Mat[2][7] + Mat[2][8] + Mat[2][9] + Mat[2][10] + Mat[2][11] + Mat[2][12] + Mat[2][13] + Mat[2][14] + eol
                                 + Mat[3][0] + Mat[3][1] + Mat[3][2] + Mat[3][3] + Mat[3][4] + Mat[3][5] + Mat[3][6] + Mat[3][7] + Mat[3][8] + Mat[3][9] + Mat[3][10] + Mat[3][11] + Mat[3][12] + Mat[3][13] + Mat[3][14] + eol
                                 + Mat[4][0] + Mat[4][1] + Mat[4][2] + Mat[4][3] + Mat[4][4] + Mat[4][5] + Mat[4][6] + Mat[4][7] + Mat[4][8] + Mat[4][9] + Mat[4][10] + Mat[4][11] + Mat[4][12] + Mat[4][13] + Mat[4][14] + eol
                                 + Mat[5][0] + Mat[5][1] + Mat[5][2] + Mat[5][3] + Mat[5][4] + Mat[5][5] + Mat[5][6] + Mat[5][7] + Mat[5][8] + Mat[5][9] + Mat[5][10] + Mat[5][11] + Mat[5][12] + Mat[5][13] + Mat[5][14] + eol
                                 + Mat[6][0] + Mat[6][1] + Mat[6][2] + Mat[6][3] + Mat[6][4] + Mat[6][5] + Mat[6][6] + Mat[6][7] + Mat[6][8] + Mat[6][9] + Mat[6][10] + Mat[6][11] + Mat[6][12] + Mat[6][13] + Mat[6][14];
                //Nome do caminho e arquivo (Neste caso colocamos na pasta do programa)
                File file = new File("jogo.txt");

                String mycontent = Mat[0][0] + Mat[0][1] + Mat[0][2] + Mat[0][3] + Mat[0][4] + Mat[0][5] + Mat[0][6] + Mat[0][7] + Mat[0][8] + Mat[0][9] + Mat[0][10] + Mat[0][11] + Mat[0][12] + Mat[0][13] + Mat[0][14] + eol
                        + Mat[1][0] + Mat[1][1] + Mat[1][2] + Mat[1][3] + Mat[1][4] + Mat[1][5] + Mat[1][6] + Mat[1][7] + Mat[1][8] + Mat[1][9] + Mat[1][10] + Mat[1][11] + Mat[1][12] + Mat[1][13] + Mat[1][14] + eol
                        + Mat[2][0] + Mat[2][1] + Mat[2][2] + Mat[2][3] + Mat[2][4] + Mat[2][5] + Mat[2][6] + Mat[2][7] + Mat[2][8] + Mat[2][9] + Mat[2][10] + Mat[2][11] + Mat[2][12] + Mat[2][13] + Mat[2][14] + eol
                        + Mat[3][0] + Mat[3][1] + Mat[3][2] + Mat[3][3] + Mat[3][4] + Mat[3][5] + Mat[3][6] + Mat[3][7] + Mat[3][8] + Mat[3][9] + Mat[3][10] + Mat[3][11] + Mat[3][12] + Mat[3][13] + Mat[3][14] + eol
                        + Mat[4][0] + Mat[4][1] + Mat[4][2] + Mat[4][3] + Mat[4][4] + Mat[4][5] + Mat[4][6] + Mat[4][7] + Mat[4][8] + Mat[4][9] + Mat[4][10] + Mat[4][11] + Mat[4][12] + Mat[4][13] + Mat[4][14] + eol
                        + Mat[5][0] + Mat[5][1] + Mat[5][2] + Mat[5][3] + Mat[5][4] + Mat[5][5] + Mat[5][6] + Mat[5][7] + Mat[5][8] + Mat[5][9] + Mat[5][10] + Mat[5][11] + Mat[5][12] + Mat[5][13] + Mat[5][14] + eol
                        + Mat[6][0] + Mat[6][1] + Mat[6][2] + Mat[6][3] + Mat[6][4] + Mat[6][5] + Mat[6][6] + Mat[6][7] + Mat[6][8] + Mat[6][9] + Mat[6][10] + Mat[6][11] + Mat[6][12] + Mat[6][13] + Mat[6][14];



                /* Cria o ficheiro caso o ficheiro não exista*/
                if (!file.exists()) {
                    file.createNewFile();
                }

            // Vai escrever no ficheiro
            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            bw.write(conteudo);
            System.out.println("Ficheiro guardado com sucesso");

            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally {
                try {
                    if (bw != null) {
                        bw.close();
                    }
                } catch (Exception ex) {
                    System.out.println("Erro ao fechar" + ex);
                }
            }
            System.exit(0);

        }
        
        // Vai verificar o que foi digitado pelo utilizador. 
        // Se for inserida uma coluna invalida (Diferente de 1 e 7) vai ser pedido ao jogador para voltar a digitar

        } else if (scan != 1

                && scan != 2
                && scan != 3
                && scan != 4
                && scan != 5
                && scan != 6
                && scan != 7) {
            do {
                System.out.println(t.getj1Nome() + " insira uma coluna valida entre 1 e 7");
                scan = scanner.nextInt();
            } while (scan != 1
                    && scan != 2
                    && scan != 3
                    && scan != 4
                    && scan != 5
                    && scan != 6
                    && scan != 7);
        }

        //Converte os numeros das colunas inseridos 1-2-3-4-5-6-7 
        //para 1-3-5-7-9-11-13
        int c = (2 * scan + 1) - 2;
        boolean adp = false;

        // Vai percorrer cada linha da coluna de baixo para cima
        // Até encontrar um espaço vazio, para inserir
        do {
            for (int i = 5; i >= 0; i--) {
                if (Mat[i][c] == " ") {
                    Mat[i][c] = "1";
                    adp = true;
                    break;
                }
            }
            // Vai verificar se a coluna introduzida pelo jogador está cheia. 
            // Se sim, vai pedir para o jogador introduzir outra coluna.
            if (adp == false) {
                System.out.println(t.getj1Nome() + " a coluna está cheia. Insira outra coluna");
                scan = scanner.nextInt();
                c = (2 * scan + 1) - 2;
                adp = false;
            }
        } while (adp == false);
    }


    /*--
        Vai inserir na linha vazia mais baixa da coluna selecionada para o segundo jogador
    */

    //Insere na linha vazia mais baixa da coluna selecionada para o segundo jogador

    public static void inserirLinhaSegundoJogador(String[][] Mat, Tabuleiro t) {

        int scan = 0;
        Scanner scanner = new Scanner(System.in);
        //Le o valor introduzido pelo utilizador
        System.out.println(t.getj2Nome() + " insira uma coluna: ");
        scan = scanner.nextInt();

        if (scan == 0) {
            System.out.println("Fechar");
            String eol = System.getProperty("line.separator");

            BufferedWriter bw = null;
            try {

                String conteudo = Mat[0][0] + Mat[0][1] + Mat[0][2] + Mat[0][3] + Mat[0][4] + Mat[0][5] + Mat[0][6] + Mat[0][7] + Mat[0][8] + Mat[0][9] + Mat[0][10] + Mat[0][11] + Mat[0][12] + Mat[0][13] + Mat[0][14] + eol
                                 + Mat[1][0] + Mat[1][1] + Mat[1][2] + Mat[1][3] + Mat[1][4] + Mat[1][5] + Mat[1][6] + Mat[1][7] + Mat[1][8] + Mat[1][9] + Mat[1][10] + Mat[1][11] + Mat[1][12] + Mat[1][13] + Mat[1][14] + eol
                                 + Mat[2][0] + Mat[2][1] + Mat[2][2] + Mat[2][3] + Mat[2][4] + Mat[2][5] + Mat[2][6] + Mat[2][7] + Mat[2][8] + Mat[2][9] + Mat[2][10] + Mat[2][11] + Mat[2][12] + Mat[2][13] + Mat[2][14] + eol
                                 + Mat[3][0] + Mat[3][1] + Mat[3][2] + Mat[3][3] + Mat[3][4] + Mat[3][5] + Mat[3][6] + Mat[3][7] + Mat[3][8] + Mat[3][9] + Mat[3][10] + Mat[3][11] + Mat[3][12] + Mat[3][13] + Mat[3][14] + eol
                                 + Mat[4][0] + Mat[4][1] + Mat[4][2] + Mat[4][3] + Mat[4][4] + Mat[4][5] + Mat[4][6] + Mat[4][7] + Mat[4][8] + Mat[4][9] + Mat[4][10] + Mat[4][11] + Mat[4][12] + Mat[4][13] + Mat[4][14] + eol
                                 + Mat[5][0] + Mat[5][1] + Mat[5][2] + Mat[5][3] + Mat[5][4] + Mat[5][5] + Mat[5][6] + Mat[5][7] + Mat[5][8] + Mat[5][9] + Mat[5][10] + Mat[5][11] + Mat[5][12] + Mat[5][13] + Mat[5][14] + eol
                                 + Mat[6][0] + Mat[6][1] + Mat[6][2] + Mat[6][3] + Mat[6][4] + Mat[6][5] + Mat[6][6] + Mat[6][7] + Mat[6][8] + Mat[6][9] + Mat[6][10] + Mat[6][11] + Mat[6][12] + Mat[6][13] + Mat[6][14];

                String mycontent = Mat[0][0] + Mat[0][1] + Mat[0][2] + Mat[0][3] + Mat[0][4] + Mat[0][5] + Mat[0][6] + Mat[0][7] + Mat[0][8] + Mat[0][9] + Mat[0][10] + Mat[0][11] + Mat[0][12] + Mat[0][13] + Mat[0][14] + eol
                        + Mat[1][0] + Mat[1][1] + Mat[1][2] + Mat[1][3] + Mat[1][4] + Mat[1][5] + Mat[1][6] + Mat[1][7] + Mat[1][8] + Mat[1][9] + Mat[1][10] + Mat[1][11] + Mat[1][12] + Mat[1][13] + Mat[1][14] + eol
                        + Mat[2][0] + Mat[2][1] + Mat[2][2] + Mat[2][3] + Mat[2][4] + Mat[2][5] + Mat[2][6] + Mat[2][7] + Mat[2][8] + Mat[2][9] + Mat[2][10] + Mat[2][11] + Mat[2][12] + Mat[2][13] + Mat[2][14] + eol
                        + Mat[3][0] + Mat[3][1] + Mat[3][2] + Mat[3][3] + Mat[3][4] + Mat[3][5] + Mat[3][6] + Mat[3][7] + Mat[3][8] + Mat[3][9] + Mat[3][10] + Mat[3][11] + Mat[3][12] + Mat[3][13] + Mat[3][14] + eol
                        + Mat[4][0] + Mat[4][1] + Mat[4][2] + Mat[4][3] + Mat[4][4] + Mat[4][5] + Mat[4][6] + Mat[4][7] + Mat[4][8] + Mat[4][9] + Mat[4][10] + Mat[4][11] + Mat[4][12] + Mat[4][13] + Mat[4][14] + eol
                        + Mat[5][0] + Mat[5][1] + Mat[5][2] + Mat[5][3] + Mat[5][4] + Mat[5][5] + Mat[5][6] + Mat[5][7] + Mat[5][8] + Mat[5][9] + Mat[5][10] + Mat[5][11] + Mat[5][12] + Mat[5][13] + Mat[5][14] + eol
                        + Mat[6][0] + Mat[6][1] + Mat[6][2] + Mat[6][3] + Mat[6][4] + Mat[6][5] + Mat[6][6] + Mat[6][7] + Mat[6][8] + Mat[6][9] + Mat[6][10] + Mat[6][11] + Mat[6][12] + Mat[6][13] + Mat[6][14];

                //Nome do caminho e arquivo
                File file = new File("jogo.txt");

                /* Cria o ficheiro caso o ficheiro não exista*/
                if (!file.exists()) {
                    file.createNewFile();
                }


            FileWriter fw = new FileWriter(file);
            bw = new BufferedWriter(fw);
            bw.write(conteudo);
            System.out.println("Ficheiro guardado com sucesso");

            } catch (IOException ioe) {
                ioe.printStackTrace();
            } finally {
                try {
                    if (bw != null) {
                        bw.close();
                    }
                } catch (Exception ex) {
                    System.out.println("Erro ao fechar" + ex);
                }
            }
            System.exit(0);
        } else if (scan != 1
                && scan != 2
                && scan != 3
                && scan != 4
                && scan != 5
                && scan != 6
                && scan != 7) {
            do {
                System.out.println(t.getj2Nome() + " insira uma coluna valida entre 1 e 7");
                scan = scanner.nextInt();
            } while (scan != 1
                    && scan != 2
                    && scan != 3
                    && scan != 4
                    && scan != 5
                    && scan != 6
                    && scan != 7);
        }

        int c = (2 * scan + 1) - 2;
        boolean adp = false;

        do {
            for (int i = 5; i >= 0; i--) {
                if (Mat[i][c] == " ") {
                    Mat[i][c] = "2";
                    adp = true;
                    break;
                }
            }
            // Vai verificar se a coluna introduzida pelo jogador está cheia. 
            // Se sim, vai pedir para o jogador introduzir outra coluna.
            if (adp == false) {
                System.out.println(t.getj2Nome() + " a coluna está cheia. Insira outra coluna");
                scan = scanner.nextInt();
                c = (2 * scan + 1) - 2;
                adp = false;
            }
        } while (adp == false);
    }

    /*--
        Verifica se o jogador1 ou o jogador2 venceram o jogo ou se o jogo ficou empatado
    */
    public static String verificarVencedor(String[][] Mat) {
        String empate = "3";

        // Verifica se existe um vencedor numa linha horizontal
        for (int i = 0; i < 6; i++) {
            for (int j = 0; j < 7; j += 2) {
                if ((Mat[i][j + 1] != " ")
                        && (Mat[i][j + 3] != " ")
                        && (Mat[i][j + 5] != " ")
                        && (Mat[i][j + 7] != " ")
                        && ((Mat[i][j + 1] == Mat[i][j + 3])
                        && (Mat[i][j + 3] == Mat[i][j + 5])
                        && (Mat[i][j + 5] == Mat[i][j + 7]))) // Se for encontrado um vencedor, vamos retornar o vencedor
                {
                    return Mat[i][j + 1];
                }
            }
        }

        // Verifica se existe um vencedor numa linha vertical
        for (int i = 1; i < 15; i += 2) {
            for (int j = 0; j < 3; j++) {
                if ((Mat[j][i] != " ")
                        && (Mat[j + 1][i] != " ")
                        && (Mat[j + 2][i] != " ")
                        && (Mat[j + 3][i] != " ")
                        && ((Mat[j][i] == Mat[j + 1][i])
                        && (Mat[j + 1][i] == Mat[j + 2][i])
                        && (Mat[j + 2][i] == Mat[j + 3][i]))) {
                    return Mat[j][i];
                }
            }
        }

        // Verifica na linha diagonal esquerda-cima e direita-baixo se existe um vencedor
        for (int i = 0; i < 3; i++) {
            for (int j = 1; j < 9; j += 2) {
                if ((Mat[i][j] != " ")
                        && (Mat[i + 1][j + 2] != " ")
                        && (Mat[i + 2][j + 4] != " ")
                        && (Mat[i + 3][j + 6] != " ")
                        && ((Mat[i][j] == Mat[i + 1][j + 2])
                        && (Mat[i + 1][j + 2] == Mat[i + 2][j + 4])
                        && (Mat[i + 2][j + 4] == Mat[i + 3][j + 6]))) {
                    return Mat[i][j];
                }
            }
        }

        // Verifica na linha diagonal direita-cima e esquerda baixo se existe um vencedor
        for (int i = 0; i < 3; i++) {
            for (int j = 7; j < 15; j += 2) {
                if ((Mat[i][j] != " ")
                        && (Mat[i + 1][j - 2] != " ")
                        && (Mat[i + 2][j - 4] != " ")
                        && (Mat[i + 3][j - 6] != " ")
                        && ((Mat[i][j] == Mat[i + 1][j - 2])
                        && (Mat[i + 1][j - 2] == Mat[i + 2][j - 4])
                        && (Mat[i + 2][j - 4] == Mat[i + 3][j - 6]))) {
                    return Mat[i][j];
                }
            }
        }

        // Verifica se o jogo ficou empatado
        for (int i = 1; i < 15; i += 2) {
            for (int j = 0; j < 1; j++) {
                if ((Mat[j][i] != " ")
                        && (Mat[j + 1][i] != " ")
                        && (Mat[j + 2][i] != " ")
                        && (Mat[j + 3][i] != " ")
                        && (Mat[j + 4][i] != " ")
                        && (Mat[j + 5][i] != " ")
                        && (Mat[j + 6][i] != " ")
                        && (Mat[j][i + 2] != " ")
                        && (Mat[j][i + 4] != " ")
                        && (Mat[j][i + 6] != " ")
                        && (Mat[j][i + 8] != " ")
                        && (Mat[j][i + 10] != " ")
                        && (Mat[j][i + 12] != " ")) {
                    return empate;
                }
            }
        }

        //Indica que não foi encontrado nenhum vencedor
        return null;
    }
    
    public void tab(Tabuleiro t) {
        String[][] Mat = criarPadrao();
        //Condição para o jogo continuar
        boolean loop = true;
        //Contador para verificar de quem é a vez
        int count = 0;
        imprimirPadrao(Mat);
        while (loop) {
            //Definimos que o 1º jogador é o primeiro a jogar, e assim em diante 
            if (count % 2 == 0) {
                inserirLinhaPrimeiroJogador(Mat, t);
            } else {
                inserirLinhaSegundoJogador(Mat, t);
            }
            count++;
            imprimirPadrao(Mat);
            
            //Verificamos se existe um vencedor em cada jogada efetuada
            if (verificarVencedor(Mat) != null) {
                if (verificarVencedor(Mat) == "1") {
                    System.out.println("O(A) jogador(a) " + t.getj1Nome() + " venceu.");
                   /* try {
                        this.i = 0;
                        Scanner linhas = new Scanner(new File("Jogadores\\Info_Jogadores.txt"));
                        while (linhas.hasNextLine()) {
                            this.split = linhas.nextLine().split(";");
                            if (this.split[1].equals("A")) {
                                this.i++;
                            }
                        }
                        
                        String[][] matJ = new String[this.i][6];
                        
                        Scanner linhas2 = new Scanner(new File("Jogadores\\Info_Jogadores.txt"));
                        this.i = 0;
                        while (linhas2.hasNextLine()) {

                            this.split = linhas2.nextLine().split(";");

                            if (this.split[1].equals("A")) {
                                matJ[this.i][0] = this.split[0];
                                matJ[this.i][1] = this.split[1];
                                matJ[this.i][2] = this.split[2];
                                matJ[this.i][3] = this.split[3];
                                matJ[this.i][4] = this.split[4];
                                matJ[this.i][5] = this.split[5];
                                matJ[this.i][6] = this.split[6];
                                this.i++;
                            }
                          //  if (t.getj1ID() == matJ[this.i][0]){
                                
                            //}
                        }
                    } catch (IOException e) {
                    }*/
                } else if (verificarVencedor(Mat) == "2") {
                    System.out.println("O(A) jogador(a) " + t.getj2Nome() + " venceu.");
                } else if (verificarVencedor(Mat) == "3") {
                    System.out.println("Empate");
                }
                //Se algum jogador vencer, acabamos com o jogo
                loop = false;
            }
        }
    }
    
    public void tabCarregarJogo(Tabuleiro cj) {
        String[][] Mat = new String [7][15];
        //Condição para o jogo continuar
        boolean loop = true;
        //Contador para verificar de quem é a vez
        int count = 0;
        imprimirPadrao(Mat);
        while (loop) {
            //Definimos que o 1º jogador é o primeiro a jogar, e assim em diante 
            if (count % 2 == 0) {
                inserirLinhaPrimeiroJogador(Mat, cj);
            } else {
                inserirLinhaSegundoJogador(Mat, cj);
            }
            count++;
            imprimirPadrao(Mat);
            
            //Verificamos se existe um vencedor em cada jogada efetuada
            if (verificarVencedor(Mat) != null) {
                if (verificarVencedor(Mat) == "1") {
                    System.out.println("O jogador " + cj.getj1Nome() + " venceu.");
                } else if (verificarVencedor(Mat) == "2") {
                    System.out.println("O jogador " + cj.getj2Nome() + " venceu.");
                } else if (verificarVencedor(Mat) == "3") {
                    System.out.println("Empate");
                }
                //Se algum jogador vencer, acabamos com o jogo
                loop = false;
            }
        }
    }
    
    public String getj1Nome() {
        return this.j1Nome;
    }

    public String getj2Nome() {
        return this.j2Nome;
    }
    
    public int getj1ID() {
        return this.j1ID;
    }
    
    public int getj2ID() {
        return this.j2ID;
    }
}
